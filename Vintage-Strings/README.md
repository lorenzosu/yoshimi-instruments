'Vintage Strings' instruments for Yoshimi (https://yoshimi.sourceforge.io/)

Vintage because:
- They (hopefully) sound 'vintage'
- They use mostly pulse (ish) waveforms
- They heavily rely on chorus(es) (potentially to be tweaked depending on use)
- They are intentionally a bit 'raw'.. A bit of a canvas to further tweak and adjust depending on need

Four of the instruments use only one base waveform with the AddSynth; one uses the PadSynth.

The test FLAC audio file shows some of the possibilities both 'solo' and layered in a few ways:

The repository also contains the Rosegarden and Yoshimi state file for the test.

Hope these can be interesting / useful for some other fellow Yoshimians ;-)
